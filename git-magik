#!/usr/bin/env bash

# $HOME/bin/login/git-magik
# Lorin Ricker: bash path functions

shF="$( basename ${0#-} ) - git-magik"
Ident="${shF}  # (LMR version 4.4 of 10/20/2019)"
[ "$DEBUGMODE" \> "0" ] && echo "%login-I, ${Ident}"

# For use in Ruby script $rby/gitsync.rb --
# my default list of git repos to check/sync --
export FREQGITREPOLIST="$wallowa $lostine $lgi $glr $vmscom $ukiah $atom"
# Also, here's the complete list (must be kept up-to-date) --
export DOCGITREPOLIST="$minam $crisi $dio $wrt $vaxscan"
export ALLGITREPOLIST="${FREQGITREPOLIST} ${DOCGITREPOLIST} $gnv $lakota $resume $prb $rbyimm $rmaster $vmshist $vmsstash $vmsvast"

# use: $ gitshow
#
# Returns a report of local git ~/.gitconfig, including
#   git remote -v show, git fetch origin and git status
gitshow()
  {
    BOLD="\033[1m"
    NORM="\033[0m"
    # lsfunction -f git%
    # echo -e "\n\$ ${BOLD}git config --list${NORM}"
    # git config --list
    echo -e "\n# ${BOLD}Global gitconfig${NORM}"    # ~/.gitconfig -> ~/bin/gitconfig)
    /bin/ls -la ~/.gitconfig
    /bin/cat ~/.gitconfig
    echo -e "\n# ${BOLD}Global gitattributes${NORM}" # ~/.gitattributes -> ~/bin/gitattributes
    /bin/ls -la ~/.gitattributes
    /bin/cat ~/.gitattributes
    echo -e "\n# ${BOLD}Global gitignore${NORM}"     # ~/.global_gitignore -> /bin/global_gitignore
    /bin/ls -la ~/.global_gitignore
    /bin/cat ~/.global_gitignore
    echo -e "\n\$ ${BOLD}git remote -v show${NORM}"
    git remote -v show
  }
export -f gitshow

# use: $ gitstat
#
# Returns a report of local git ~/.gitconfig, including
#   git remote -v show, git fetch origin and git status
gitstat()
  {
    BOLD="\033[1m"
    NORM="\033[0m"
    echo -e "\n\$ ${BOLD}git fetch origin${NORM}"
    git fetch origin
    echo -e "\n\$ ${BOLD}git status${NORM}"
    git status
  }
export -f gitstat

# use: $ gitlists
#
# Returns a report of local git ~/.gitconfig, including
#   git remote -v show, git fetch origin and git status
gitlists()
  {
    ULINE="\033[4m"
    NORM="\033[0m"
    f="$HOME/bin/pplist"
    if [[ -f "$f" ]]; then
      echo -e "\n${ULINE}FREQGITREPOLIST${NORM}"
      pplist $FREQGITREPOLIST
      echo -e "\n${ULINE}DOCGITREPOLIST${NORM}"
      pplist $DOCGITREPOLIST
      echo -e "\n${ULINE}ALLGITREPOLIST${NORM}"
      pplist $ALLGITREPOLIST
    else
      echo -e "\n${ULINE}FREQGITREPOLIST${NORM}"
      echo "  ${FREQGITREPOLIST}"
      echo -e "\n${ULINE}DOCGITREPOLIST${NORM}"
      echo "  ${DOCGITREPOLIST}"
      echo -e "\n${ULINE}ALLGITREPOLIST${NORM}"
      echo "  ${ALLGITREPOLIST}"
    fi
  }
export -f gitlists

# use: $ gitconf
#
# Returns a report of local git ~/.gitconfig, including
#   git remote -v show, git fetch origin and git status
gitconf()
  {
    gitshow
    gitstat
  }
export -f gitconf

alias gitk="/usr/bin/gitk &"
